package parser;

import com.google.common.io.Resources;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class Main extends Application {

    private File root;
    private File extractedDriver;

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();

        InputStream chromeDriverExe = getClass().getClassLoader().getResourceAsStream("chromedriver.exe");

        if(chromeDriverExe!=null) {

            this.root = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();

            this.extractedDriver = new File(root+"/chromedriver.exe");

            if(!extractedDriver.exists()) {
                FileOutputStream fileOutputStream = new FileOutputStream(extractedDriver);
                IOUtils.copy(chromeDriverExe, fileOutputStream);
            }

            loader.setLocation(Resources.getResource("window.fxml"));

        } else {
            loader.setLocation(Resources.getResource("error.fxml"));
        }

        Parent root = loader.load();

        if(loader.getController()!=null) {
            Controller controller = loader.getController();
            controller.setMain(this);
        }

        primaryStage.setTitle("LDJOBSPARSER");

        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }

    public File getExtractedDriver() {
        return extractedDriver;
    }

    public File getRoot() {
        return root;
    }
}
