package parser;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.opencsv.CSVWriter;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

import java.io.*;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {

    public static String LINKEDIN_JOBS_REGEXP = "https://www\\.linkedin\\.com/jobs/search/(.*)";

    private Main main;

    private Driver driver;

    @FXML
    private TextField email;

    @FXML
    private TextField password;

    @FXML
    private TextField url;

    @FXML
    private TextField separator;

    @FXML
    private Button start;

    @FXML
    private Button stop;

    @FXML
    private ListView log;

    @FXML
    private Label viewPage;

    @FXML
    private Label viewParsedCount;

    @FXML
    private Button logToFile;

    private Logger logger;

    @FXML
    private void initialize() {

        stop.setDisable(true);

        this.logger = new Logger(log);

        start.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if(!email.getText().isEmpty()) {
                    if(!password.getText().isEmpty()) {
                        if(!url.getText().isEmpty() && url.getText().matches(LINKEDIN_JOBS_REGEXP)) {

                            if(separator.getText().isEmpty()) {
                                separator.setText(";");
                            } else {
                                separator.setText(String.valueOf(separator.getText().charAt(0)));
                            }

                            stop.setDisable(false);

                            start.setDisable(true);

                            separator.setDisable(true);

                            email.setDisable(true);
                            password.setDisable(true);
                            url.setDisable(true);

                            log.getItems().clear();

                            driver = new Driver(email.getText(), password.getText(), url.getText(), main.getExtractedDriver());

                            driver.start();

                        } else {
                            logger.error("URL is empty or incorrect. Try again");
                        }
                    } else {
                        logger.error("Enter Password");
                    }
                } else {
                    logger.error("Enter E-Mail");
                }
            }
        });

        stop.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(driver!=null) {
                    driver.stopProcess();
                }
            }
        });

        logToFile.setOnMouseClicked(event -> {
            try {
                FileUtils.writeLines(new File(main.getRoot()+"/ldJobsParser.log"), "UTF-8", log.getItems(), "\n", true);
                logger.info("Log file Created - "+main.getRoot()+"/ldJobsParser.log");
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("error created log file");
            }
        });

    }

    private void stopping() {
        stop.setDisable(true);
        start.setDisable(false);
        email.setDisable(false);
        password.setDisable(false);
        url.setDisable(false);
        separator.setDisable(false);
    }

    private void viewPage(int page) {
        Platform.runLater(() -> {
            viewPage.setText(page>-1?String.valueOf(page):"");
        });
    }

    private void viewParsedCount(int count) {
        Platform.runLater(() -> {
            viewParsedCount.setText(count>-1?String.valueOf(count):"");
        });
    }

    void setMain(Main main) {
        this.main = main;
    }

    private class Driver extends Thread {

        private static final String START = "&start=";

        private final Pattern pattern = Pattern.compile("&start=([0-9]+)");

        private static final String JOBS_CSV_FILE = "jobs.csv";
        private static final String LINKEDIN_LOGIN_URL = "https://www.linkedin.com/";
        private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";

        private final String[] JOBS_HEADERS = new String[]{"Name", "Logo", "Company name", "Company link", "Location", "Industry", "Info"};

        private int page = 0;

        private int parsedCount = 0;

        private String email;
        private String password;
        private String url;

        private File driverPath;

        private boolean stop = false;

        private CSVWriter csvWriter;
        private CSVPrinter csvPrinter;

        private String mainWindowHandle;

        private ChromeDriver chromeDriver;

        private JavascriptExecutor javascriptExecutor;

        private ChromeDriverService chromeDriverService;

        private ChromeOptions options = new ChromeOptions();

        Driver(String email, String password, String url, File driverPath) {

            logger.info("Loading");

            this.driverPath = driverPath;

            this.email = email;
            this.password = password;

            this.url = url;

            Matcher m = pattern.matcher(this.url);

            if(m.find()) {
                try {
                    this.page = Integer.parseInt(m.group(1));
                    if(this.page % 25 != 0) {
                        this.page = 0;
                    }
                } catch (NumberFormatException e) {
                    this.page = 0;
                }

                this.url = this.url.replaceAll(pattern.pattern(), "");



            }

            logger.info("Use url - "+this.url);

            System.setProperty("webdriver.chrome.driver", this.driverPath.getAbsolutePath());
            System.setProperty("webdriver.chrome.logfile", main.getRoot()+"/log.log");
            System.setProperty("webdriver.chrome.verboseLogging", "true");

            options.addArguments("start-maximized");
            options.addArguments("--user-agent="+USER_AGENT);

            options.setCapability("chrome.verbose", true);
            options.setCapability("networkConnectionEnabled", true);
            options.setCapability("takesScreenshot", true);
            options.setCapability("--user-agent", USER_AGENT);

            options.setCapability("browserName", "JobsParser");
            options.setCapability("platform", org.openqa.selenium.Platform.WINDOWS);
            options.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
            options.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR, true);
            options.setCapability(CapabilityType.SUPPORTS_FINDING_BY_CSS, true);
            options.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);

            LoggingPreferences logPrefs = new LoggingPreferences();
            logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
            logPrefs.enable(LogType.PROFILER, Level.INFO);
            logPrefs.enable(LogType.BROWSER, Level.INFO);
            logPrefs.enable(LogType.CLIENT, Level.INFO);
            logPrefs.enable(LogType.DRIVER, Level.INFO);
            logPrefs.enable(LogType.SERVER, Level.INFO);
            options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);

            chromeDriverService = new ChromeDriverService.Builder().usingAnyFreePort()
                    .usingDriverExecutable(this.driverPath)
                    .withLogFile(new File(main.getRoot()+"/log.log")).build();

        }

        @Override
        public void run() {
            Writer writer = null;

            try {

                logger.info("Create template csv file");

                Path csvPath = Paths.get(main.getRoot() + "/" + JOBS_CSV_FILE);

                File csv = csvPath.toFile();

                if (!csv.exists() || (csv.exists() && csv.canExecute() && csv.canRead() && csv.canWrite())) {

                    logger.info("CSV separator - " + separator.getText());

                    Files.deleteIfExists(csvPath);

                    writer = Files.newBufferedWriter(csvPath);

                    csvWriter = new CSVWriter(writer,
                            separator.getText().charAt(0),
                            CSVWriter.NO_QUOTE_CHARACTER,
                            CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                            CSVWriter.DEFAULT_LINE_END);

                    csvWriter.writeNext(JOBS_HEADERS, false);

                    logger.info("CSV file created - " + csvPath);

                    logger.info("Starting chrome driver");

                    chromeDriver = new ChromeDriver(chromeDriverService, options);

                    javascriptExecutor = (JavascriptExecutor) chromeDriver;

                    chromeDriver.get(LINKEDIN_LOGIN_URL);

                    mainWindowHandle = chromeDriver.getWindowHandle();

                    clearTabs();

                    if (login()) {
                        while (page <= 1000) {

                            if (stop) {
                                break;
                            }
                            
                            viewPage(getPageNumber());

                            logger.info("Open page - " + (getPageNumber()));
                            
                            chromeDriver.get(this.url+START+this.page);

                            Thread.sleep(5000);

                            addScrollScript();

                            Thread.sleep(3000);

                            scrollToBottom();

                            scrollToTop();

                            List<WebElement> jobs = chromeDriver.findElementsByCssSelector(".jobs-search-results__list li.card-list__item");

                            if (!jobs.isEmpty()) {
                                for (WebElement job : jobs) {
                                    try {

                                        if (stop) {
                                            break;
                                        }

                                        Thread.sleep(3000);

                                        List<WebElement> a = job.findElements(By.cssSelector(".job-card-search__upper-content-wrapper .job-card-search__link-wrapper"));

                                        if(a!=null && a.size()>=1) {

                                            if(openNewTab(a.get(0).getAttribute("href"))) {

                                                Thread.sleep(5000);

                                                addScrollScript();

                                                scrollToBottom();

                                                Thread.sleep(2000);

                                                scrollToTop();

                                                Thread.sleep(2000);

                                                WebElement viewMore = findElement(".view-more-icon");

                                                if(viewMore!=null && viewMore.isDisplayed()) {
                                                    try {
                                                        viewMore.click();
                                                    } catch (WebDriverException e){
                                                        javascriptExecutor.executeScript("arguments[0].click()", viewMore);
                                                    }
                                                    Thread.sleep(2000);
                                                } else {
                                                    System.out.println("view-more is null or not displayed");
                                                }

                                                WebElement name = findElement(".jobs-details-top-card__job-title");

                                                if(name!=null) {

                                                    WebElement companyLink = findElement(".jobs-details-top-card__company-url");

                                                    if(companyLink!=null) {

                                                        WebElement logo = findElement( ".jobs-details-top-card__company-logo");

                                                        if(logo!=null) {

                                                            WebElement companyLocation = findElement(".jobs-details-top-card__bullet");

                                                            if(companyLocation!=null) {

                                                                WebElement info = findElement(".jobs-description-content__text");

                                                                if(info!=null) {

                                                                    String industry = "";
                                                                    List<WebElement> industries = chromeDriver.findElements(By.cssSelector(".jobs-box__list.jobs-description-details__list.js-formatted-industries-list li a"));

                                                                    for(WebElement webElement: industries) {
                                                                        industry+=webElement.getText()+" ";
                                                                    }

                                                                    try {

                                                                        write(name.getText(), logo.getAttribute("src"), companyLink.getText(),
                                                                                companyLink.getAttribute("href"),
                                                                                escape(companyLocation.getText()),
                                                                                industry,
                                                                                escape(info.getText()));

                                                                        /*.replaceAll("Регион компании\n|Company Location\n", "")*/

                                                                        parsedCount++;

                                                                        viewParsedCount(parsedCount);

                                                                        Thread.sleep(2000);

                                                                    } catch (Exception e) {
                                                                        logger.error(exceptionToString(e));
                                                                    }

                                                                } else {
                                                                    logger.error("info not found");
                                                                }

                                                            } else {
                                                                logger.error("companyLocation not found");
                                                            }

                                                        } else {
                                                            logger.error("logo not found");
                                                        }

                                                    } else {
                                                        logger.error("companyLink not found");
                                                    }

                                                } else {
                                                    logger.error("name not found");
                                                }

                                                clearTabs();

                                            } else {
                                                logger.error("href not found");
                                            }
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        logger.error(exceptionToString(e));
                                    }

                                }
                            }

                            page += 25;
                        }
                    } else {
                        logger.info("Login is not success: Captcha, Code");
                    }

                } else {
                    logger.error("Cant delete or write csv file. Please close it.");
                }
            } catch (FileSystemException e) {
                logger.error("Cant delete or write csv file. Please close it.");
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(exceptionToString(e));
            } finally {
                if(csvWriter!=null) {
                    try {
                        csvWriter.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            quit();
        }

        private int getPageNumber() {
            return page/25+1;
        }

        private void quit() {

            logger.info("Quit");

            viewPage(-1);
            viewParsedCount(-1);

            if(csvPrinter!=null) {
                try {
                    csvPrinter.flush();
                    csvPrinter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(chromeDriver!=null) {

                try {

                    logger.info("Try logout");

                    WebElement logoutButton = findElement("#nav-settings__dropdown-trigger");

                    if(logoutButton!=null) {

                        logoutButton.click();

                        Thread.sleep(1000);

                        WebElement btn = findElement("[data-control-name=\"nav.settings_signout\"]");

                        if(btn!=null) {
                            btn.click();
                            Thread.sleep(2000);
                        } else {
                            logger.info("Logout is fail: button not found");
                        }

                    } else {
                        logger.info("Logout is fail: menu button not found");
                    }

                } catch (Exception e) {
                    logger.info("Logout is fail");
                }

                chromeDriver.quit();
            }

            stopping();

        }

        void stopProcess() {
            stop = true;
            logger.info("Stopping. Wait...");
        }

        private void clearTabs() {
            ArrayList<String> TABS = new ArrayList<String>(chromeDriver.getWindowHandles());
            if (TABS.size() > 1) {
                for (String TAB : TABS) {
                    if (!TAB.equals(mainWindowHandle)) {
                        chromeDriver.switchTo().window(TAB);
                        chromeDriver.close();
                    }
                }
                chromeDriver.switchTo().window(mainWindowHandle);
            }
        }

        private boolean openNewTab(String href) throws InterruptedException {

            if(!href.isEmpty()) {

                javascriptExecutor.executeScript("window.open('" + href + "','_blank');");

                Thread.sleep(1500);

                ArrayList<String> tabs = new ArrayList<String>(chromeDriver.getWindowHandles());

                if (tabs.size() > 1) {
                    for (String tab : tabs) {
                        if (!tab.equals(mainWindowHandle)) {
                            chromeDriver.switchTo().window(tab);
                            return true;
                        }
                    }
                }

            }

            return false;

        }

        private void addScrollScript() {
            try {
                javascriptExecutor.executeScript(Resources.toString(Resources.getResource("scroll.js"), Charsets.UTF_8));
                Thread.sleep(1000);
            } catch(IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void scrollToTop() throws InterruptedException {
            if(!stop) {
                javascriptExecutor.executeScript("jobsScrollTo(0, 1000);");
                Thread.sleep(1000);
            }
        }

        private void scrollToBottom() throws InterruptedException {
            if(!stop) {
                javascriptExecutor.executeScript("jobsScrollTo(document.body.scrollHeight, 1000);");
                Thread.sleep(1000);
            }
        }

        private String escape(String info){
            return info.replaceAll("[;:\"]", "").replace("\n", " ")/*.replaceAll("Описание вакансии|Job description", "")*/.trim();
        }

        private WebElement findElement(String cssSelector) {
            return chromeDriver.findElementsByCssSelector(cssSelector).stream().findFirst().orElse(null);
        }

        private WebElement findElement(WebElement webElement, String cssSelector) {
            return webElement.findElements(By.cssSelector(cssSelector)).stream().findFirst().orElse(null);
        }

        private boolean login() throws InterruptedException {

            logger.info("Login in");

            chromeDriver.findElement(By.id("login-email")).sendKeys(email);

            chromeDriver.findElement(By.id("login-password")).sendKeys(password);

            chromeDriver.findElement(By.id("login-submit")).submit();

            Thread.sleep(3000);

            return chromeDriver.getCurrentUrl().contains("feed") || chromeDriver.getCurrentUrl().contains("nhome");
        }

        private void write(String... objects) {
            logger.info("WRITE - "+ Arrays.toString(objects));
            csvWriter.writeNext(objects, false);
            /*try {
                csvPrinter.printRecords(name, logo, companyName, companyLink, location, info);
            } catch (IOException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }*/
        }

        private String exceptionToString(Throwable e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return sw.toString();
        }

    }

    private class Logger {

        private ListView log;

        Logger(ListView log) {
            this.log = log;
        }

        void error(String message) {
            Platform.runLater(() -> {
                log.getItems().add("ERROR: "+message);
                log.scrollTo(log.getItems().size()-1);
            });
        }

        void info(String message) {
            Platform.runLater(() -> {
                log.getItems().add("INFO: "+message);
                log.scrollTo(log.getItems().size()-1);
            });
        }

    }

}
