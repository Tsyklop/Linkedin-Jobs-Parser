# Linkedin-Jobs-Parser
Parsing jobs in Linkedin

## INSTALL

- Install [JRE 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
- Clone repository
- Open it in IDEA IDE
- Import dependensies
- Download (If he is not present) [ChromeDriver v2.38 or above](http://chromedriver.chromium.org/downloads) and put in in resource folder (This is important!!!)
- Run debug or create artifact.

## USAGE

- Enter E-Mail and password from your Linkedin account
- Enter jobs link
- If need type csv separator
- Press Start button.

## RESULT
All parsed jobs contains in jobs.csv near in program jar file or in target folder.
You can load all logs in file. He is will be located near jar file or in target folder.
